{
  const {
    html,
  } = Polymer;
  /**
    `<cells-bfm-expandable-table>` Description.

    Example:

    ```html
    <cells-bfm-expandable-table></cells-bfm-expandable-table>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-bfm-expandable-table | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsBfmExpandableTable extends Polymer.Element {

    static get is() {
      return 'cells-bfm-expandable-table';
    }

    static get properties() {
      return {
        title: String,
        subTitle: String,
        message: String,
        amount: {
          type: Object,
          value: () => ({})
        },
        items: {
          type: Array,
          value: () => ([])
        },
        button: {
          type: Object,
          value: () => ({})
        },
        opened: {
          type: Boolean,
          value: false
        },
        iconClosed: {
          type: String,
          value: 'coronita:expand'
        },
        iconOpened: {
          type: String,
          value: 'coronita:collapse'
        },
        _icon: {
          type: String,
          value: 'coronita:expand'
        }
      };
    }
    _handleClickHeader() {
      this.opened = !this.opened;
      let currentIcon = (this.opened) ? this.iconOpened : this.iconClosed;
      this.set('_icon', currentIcon);
    }
    _handleBtn() {
      this.dispatchEvent(new CustomEvent(this.button.event || 'click-button', {
        composed:true,
        bubbles:true,
        detail: this.button.data || '', }));
    }
  }

  customElements.define(CellsBfmExpandableTable.is, CellsBfmExpandableTable);
}